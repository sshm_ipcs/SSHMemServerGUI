#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QStringList>
#include <QStringListModel>
#include "sshmemserver.h"

#if QT_VERSION < 0x040800
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>
#include <QSpinBox>
#include <QListView>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QFileDialog>
#include <QSpinBox>
#else
QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
class QSpinBox;
class QListView;
class QVBoxLayout;
class QHBoxLayout;
class QComboBox;
class QTreeView;
class QTreeWidget;
class QTreeWidgetItem;
class QFileDialog;
class QSpinBox;

class SSHMemClient;
QT_END_NAMESPACE
#endif


enum tree_element_type {
    TE_CLIENT_ID,
    TE_SOCKET_ID,
    TE_PEER_ID,
};


typedef struct {
    enum tree_element_type type;
    int                    client_id;
} tree_element_t;
Q_DECLARE_METATYPE(tree_element_t)


#define SHM_MIN_SIZE       1         // 1KB
#define SHM_MAX_SIZE    1024 * 10    // 10MB


class Dialog : public QDialog {
    Q_OBJECT

public:
    Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
/*  Message Queue Commands  */
    void MQPingClient ();
/*  SharedMemory Commands  */
    void ShMemFlashAll ();
    void ShMemDumpAll ();
/*  Socket Commands  */
    void ServerConnect ();
    void ServerDisconnect();
/*  Socket events  */
    void NewClientConnected ();
/*  TreeWidget Client events  */
    void trw_client_clicked (QTreeWidgetItem * item, int column);

private:
    SSHMemServer   *sshm_server;
    tree_element_t selected_element;

/*  Graphics functions  */
    /*  first panel  */
    void g_createFGBoxSocketFileName ();
    void g_createFGBoxSharedMemoryConfig ();
    void g_createHGBoxSocketConn ();
    void g_createFGBoxSharedMemoryCommand();
    void g_createFGBoxMessageQueueCommand();
    void g_setSocketStatus ();

    /*  second panel  */
    void g_setTreeSystemView ();

    /*  third panel  */
    void g_setLogger ();

    /*  updater functions  */
    void update_socket_status ();
    void addLog (QString str);
    void update_client_tree ();


/*  Graphics Elements  */
    QHBoxLayout       *mainLayout;
    QVBoxLayout       *first_mainLayout;
    QVBoxLayout       *second_mainLayout;
    QVBoxLayout       *third_mainLayout;

    /*  first panel  */
    enum { NumGridRows = 3, NumButtonsConn = 2 };
    QString           str_pbn_SocketConn;
    QStringList       str_list;

    QGroupBox         *fgb_SocketSettings;
    QLabel            *lbl_SocketFileName;
    QLineEdit         *txt_SocketFileName;

    QGroupBox         *fgb_SharedMemorySettings;
    QLabel            *lbl_SharedMemoryKey;
    QLineEdit         *txt_SharedMemoryKey;
    QLabel            *lbl_SharedMemorySize;
    QSpinBox          *sbx_SharedMemorySize;

    QGroupBox         *hgb_SocketConn;
    QPushButton       *pbn_SocketConn[NumButtonsConn];

    QGroupBox         *fgb_ShMemCommands;
    QPushButton       *pbn_ShMemFlashAll;
    QPushButton       *pbn_ShMemDumpAll;

    QGroupBox         *fgb_MQCommands;
    QPushButton       *pbn_MQPingClient;
    QSpinBox          *spb_MQPingIntLine;

    QGroupBox         *fgb_SocketStatus;
    QLabel            *lbl_status_conn;
    QLabel            *lbl_status_conn_info;
    QLabel            *lbl_status_num_conn;
    QLabel            *lbl_status_num_conn_info;

    /*  second panel  */
    QGroupBox         *vgb_systemStructure;
    QTreeWidget       *trv_systemStructure;

    /*  third panel  */
    QGroupBox         *vgb_Logging;
    QListView         *ltv_Logging;
    QStringList       list;
    QStringListModel  *model;
};

#endif // DIALOG_H
