#if QT_VERSION >= 0x040800
#include <QtWidgets>
#else
#include <QWidget>
#endif

#include "dialog.h"

Dialog::Dialog(QWidget *parent) : QDialog(parent) {

    str_pbn_SocketConn = "Connect, Disconnect";

    g_createFGBoxSocketFileName ();
    g_createFGBoxSharedMemoryConfig ();
    g_createHGBoxSocketConn ();
    g_createFGBoxSharedMemoryCommand ();
    g_createFGBoxMessageQueueCommand ();
    g_setSocketStatus ();

    g_setTreeSystemView ();

    g_setLogger ();

    first_mainLayout = new QVBoxLayout ();
    second_mainLayout = new QVBoxLayout ();
    third_mainLayout = new QVBoxLayout ();

    mainLayout = new QHBoxLayout ();

    first_mainLayout->addWidget (fgb_SocketSettings);
    first_mainLayout->addWidget (fgb_SharedMemorySettings);
    first_mainLayout->addWidget (hgb_SocketConn);
    first_mainLayout->addWidget (fgb_ShMemCommands);
    first_mainLayout->addWidget (fgb_MQCommands);
    first_mainLayout->addWidget (fgb_SocketStatus);

    second_mainLayout->addWidget (vgb_systemStructure);

    third_mainLayout->addWidget (vgb_Logging);

    mainLayout->addLayout (first_mainLayout);
    mainLayout->addLayout (second_mainLayout);
    mainLayout->addLayout (third_mainLayout);

    setLayout(mainLayout);
    setWindowTitle(tr("Simnow Shared Memory SERVER GUI"));

    this->txt_SharedMemoryKey->setText (SHM_KEY);

    connect (pbn_SocketConn[0], SIGNAL(clicked(bool)),
            this, SLOT(ServerConnect()));
    connect (pbn_SocketConn[1], SIGNAL(clicked(bool)),
            this, SLOT(ServerDisconnect()));

    connect (pbn_ShMemFlashAll, SIGNAL(clicked(bool)),
             this, SLOT(ShMemFlashAll()));
    connect (pbn_ShMemDumpAll, SIGNAL(clicked(bool)),
             this, SLOT(ShMemDumpAll()));

    connect (pbn_MQPingClient, SIGNAL(clicked(bool)),
             this, SLOT(MQPingClient()));

    connect (trv_systemStructure, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
             this, SLOT(trw_client_clicked(QTreeWidgetItem*,int)));

    this->sshm_server = new SSHMemServer ();
    if ( this->sshm_server != NULL ) {

        connect (sshm_server, SIGNAL(newClientConnected()),
                 this, SLOT(NewClientConnected()));

        this->sshm_server->setSocketFileName ("sshmem");
        txt_SocketFileName->setText (this->sshm_server->getSocketFileName());
        update_socket_status ();
    } else {
        lbl_status_conn_info->setText (tr("Server error"));
    }

   this->sshm_server->start();
}


Dialog::~Dialog()
{
    this->ServerDisconnect ();
}

/*  ___________________________________________________________________
 * |                                                                   |
 * |                             GUI SETTING                           |
 * |___________________________________________________________________|
 */
void Dialog::g_createFGBoxSocketFileName () {
    fgb_SocketSettings = new QGroupBox (tr("Socket Settings"));
    lbl_SocketFileName = new QLabel (tr("Socket File Name"));
    txt_SocketFileName = new QLineEdit ();
    QFormLayout *layout = new QFormLayout;

    layout->addRow (lbl_SocketFileName, txt_SocketFileName);
    fgb_SocketSettings->setLayout (layout);
}


void Dialog::g_createFGBoxSharedMemoryConfig () {
    fgb_SharedMemorySettings = new QGroupBox (tr("SharedMemory Settings"));

    lbl_SharedMemoryKey = new QLabel (tr("key"));
    txt_SharedMemoryKey = new QLineEdit ();

    lbl_SharedMemorySize = new QLabel (tr("Size (KB)"));
    sbx_SharedMemorySize = new QSpinBox ();
    sbx_SharedMemorySize->setRange (SHM_MIN_SIZE, SHM_MAX_SIZE);
    sbx_SharedMemorySize->setSingleStep (1);
    sbx_SharedMemorySize->setValue (SHM_MIN_SIZE);

    QFormLayout *layout = new QFormLayout;

    layout->addRow (lbl_SharedMemoryKey, txt_SharedMemoryKey);
    layout->addRow (lbl_SharedMemorySize, sbx_SharedMemorySize);
    fgb_SharedMemorySettings->setLayout (layout);
}


void Dialog::g_createHGBoxSocketConn () {
    hgb_SocketConn      = new QGroupBox (tr("Socket Commands"));
    QHBoxLayout *layout = new QHBoxLayout;

    this->str_list << this->str_pbn_SocketConn.split(",");

    for ( int i = 0 ; i < NumButtonsConn ; ++i ) {
        pbn_SocketConn[i] = new QPushButton(this->str_list.at(i));
        layout->addWidget (pbn_SocketConn[i]);
    }
    hgb_SocketConn->setLayout (layout);
}


void Dialog::g_createFGBoxSharedMemoryCommand () {
    QGridLayout *layout = new QGridLayout ();
    fgb_ShMemCommands   = new QGroupBox (tr("SharedMemory Commands"));

    pbn_ShMemFlashAll   = new QPushButton (tr("Flash All"));
    pbn_ShMemDumpAll    = new QPushButton (tr("Dump All"));

    layout->addWidget (pbn_ShMemFlashAll, 0, 0, 1, 1);
    layout->addWidget (pbn_ShMemDumpAll,  0, 1, 1, 1);
    fgb_ShMemCommands->setLayout (layout);
}


void Dialog::g_createFGBoxMessageQueueCommand () {
    QGridLayout *layout = new QGridLayout ();
    fgb_MQCommands      = new QGroupBox (tr("MQ Commands"));

    pbn_MQPingClient    = new QPushButton (tr("Ping Client"));
    spb_MQPingIntLine   = new QSpinBox ();

    spb_MQPingIntLine->setRange (MIN_INT_LINE, MAX_INT_LINE);
    spb_MQPingIntLine->setSingleStep (1);
    spb_MQPingIntLine->setPrefix ("interrupt ");

    layout->addWidget (pbn_MQPingClient, 0, 0, 1, 1);
    layout->addWidget (spb_MQPingIntLine,  0, 1, 1, 1);

    fgb_MQCommands->setLayout (layout);
}


void Dialog::g_setSocketStatus () {
    QFormLayout *layout      = new QFormLayout ();
    fgb_SocketStatus         = new QGroupBox (tr("Socket Status"));

    lbl_status_conn          = new QLabel (tr("Conn. status"));
    lbl_status_num_conn      = new QLabel (tr("Client conn."));
    lbl_status_conn_info     = new QLabel ();
    lbl_status_num_conn_info = new QLabel ();

    layout->addRow (lbl_status_conn, lbl_status_conn_info);
    layout->addRow (lbl_status_num_conn, lbl_status_num_conn_info);

    fgb_SocketStatus->setLayout (layout);
}


void Dialog::g_setTreeSystemView () {
    QStringList ColumnNames;
    QVBoxLayout *layout = new QVBoxLayout;
    vgb_systemStructure = new QGroupBox (tr("System Structure"));
    trv_systemStructure = new QTreeWidget ();

    layout->addWidget (trv_systemStructure);

    vgb_systemStructure->setLayout (layout);

    ColumnNames << "Client key" << "value";
    trv_systemStructure->setColumnCount (2);
    trv_systemStructure->setHeaderLabels (ColumnNames);
}


void Dialog::g_setLogger () {
    vgb_Logging = new QGroupBox (tr("System Log"));
    QHBoxLayout *layout = new QHBoxLayout ();
    ltv_Logging = new QListView ();
    layout->addWidget (ltv_Logging);
    vgb_Logging->setLayout (layout);

    model = new QStringListModel (this);
    ltv_Logging->setModel (model);
    ltv_Logging->setEditTriggers (QAbstractItemView::NoEditTriggers);
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                                                                   |
 * |___________________________________________________________________|
 */
void Dialog::update_socket_status () {
    if ( this->sshm_server->isServerConnected () ) {
        lbl_status_conn_info->setText (tr("Connected"));
        lbl_status_num_conn_info->setText (
                    QString::number(this->sshm_server->getNumClientConnected()));
        this->addLog (tr("Server Connected"));

        this->txt_SocketFileName->setReadOnly (true);
        this->sbx_SharedMemorySize->setReadOnly (true);

        this->pbn_SocketConn[0]->setEnabled (false);
        this->pbn_SocketConn[1]->setEnabled (true);

        this->pbn_ShMemFlashAll->setEnabled (true);
        this->pbn_ShMemDumpAll->setEnabled (true);
    } else {
        lbl_status_conn_info->setText (tr("Disconnected"));
        lbl_status_num_conn_info->setText (tr("0"));
        this->addLog (tr("Server Disconnected"));

        this->txt_SocketFileName->setReadOnly (false);
        this->sbx_SharedMemorySize->setReadOnly (false);

        this->pbn_MQPingClient->setEnabled (false);
        this->spb_MQPingIntLine->setEnabled (false);

        this->pbn_SocketConn[0]->setEnabled (true);
        this->pbn_SocketConn[1]->setEnabled (false);

        this->pbn_ShMemFlashAll->setEnabled (false);
        this->pbn_ShMemDumpAll->setEnabled (false);
    }
}


void Dialog::addLog (QString str) {
    list << str;
    model->setStringList(list);
}


void Dialog::update_client_tree () {
    SSHMemPeerServer peerList = this->sshm_server->getClientStructure ();

    trv_systemStructure->clear ();

    SSHMemPeerServer::iterator iter;
    for ( iter = peerList.begin() ; iter != peerList.end() ; ++iter) {
        QVariant var_client, var_socket;
        tree_element_t te_client, te_socket;
        QVariant var_peer;
        tree_element_t te_peer;

        QTreeWidgetItem *client_root = new QTreeWidgetItem(trv_systemStructure);
        client_root->setText (0, tr("Client"));
        client_root->setText (1, QString::number(iter->id));

        te_client.type = TE_CLIENT_ID;
        te_client.client_id = iter->id;
        var_client.setValue (te_client);
        client_root->setData (0, Qt::UserRole, var_client);

        QTreeWidgetItem *client_socket = new QTreeWidgetItem ();
        client_socket->setText (0, tr("sock_id"));
        client_socket->setText (1, QString::number(iter->sock_fd));

        te_socket.type = TE_SOCKET_ID;
        te_socket.client_id = iter->id;
        var_socket.setValue (te_socket);
        client_socket->setData (0, Qt::UserRole, var_socket);

        QTreeWidgetItem *client_peerMQ = new QTreeWidgetItem ();
        client_peerMQ->setText (0, tr("msg queue"));
        client_peerMQ->setText (1, iter->message_port->get_fd_name());
        te_peer.type = TE_PEER_ID;
        te_peer.client_id = iter->id;
        var_peer.setValue (te_peer);
        client_peerMQ->setData (0, Qt::UserRole, var_peer);

        client_root->addChild (client_socket);
        client_root->addChild (client_peerMQ);
    }
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                             SOCKET SLOTS                          |
 * |___________________________________________________________________|
 */
void Dialog::ServerConnect () {
    int ret;
    this->sshm_server->setSocketFileName (this->txt_SocketFileName->text ());
    this->sshm_server->setMemSize ((quint64)this->sbx_SharedMemorySize->value() * 1024);
    this->sshm_server->setMemKey (this->txt_SharedMemoryKey->text ());

    ret = this->sshm_server->ConnectServer ();
    if ( ret < 0 ) {
        this->addLog (tr("Error on connect socket"));
    } else if ( ret > 0 ) {
        this->addLog (this->sshm_server->getMsgFromErrCode(ret));
    }
    update_socket_status ();
}


void Dialog::ServerDisconnect () {
    this->sshm_server->DisconnectServer ();
    update_socket_status ();
    update_client_tree ();
}


void Dialog::NewClientConnected () {
    update_socket_status ();

    SSHMemPeerServer peerList = sshm_server->getClientStructure ();
    update_client_tree ();
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                         SHARED MEMORY SLOTS                       |
 * |___________________________________________________________________|
 */
void Dialog::ShMemFlashAll () {
    QString fileName = QFileDialog::getOpenFileName(0, QString(), QString(),
                                        tr("binary (*.bin)"));

    this->sshm_server->flash_memory_from_file (fileName);
}


void Dialog::ShMemDumpAll () {
    QString fileName = QFileDialog::getSaveFileName(0, QString(), QString(),
                                        tr("binary (*.bin)"));

    this->sshm_server->dump_memory_to_file (fileName);
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                   MESSAGE QUEUE SLOTS / COMMANDS                  |
 * |___________________________________________________________________|
 */
void Dialog::MQPingClient () {
    int interrupt = this->spb_MQPingIntLine->value();
    int client    = this->selected_element.client_id;
    int ret       = this->sshm_server->send_PingToClient (client, interrupt);
    if ( ret == 0 )
        this->addLog (tr("Ping send"));
    else
        this->addLog (tr("Error on sending ping"));
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                      TREEWIDGET CLIENT SLOTS                      |
 * |___________________________________________________________________|
 */
void Dialog::trw_client_clicked (QTreeWidgetItem *item, int column) {
    bool en_pbn_MQPingClient = false;
    tree_element_t te;
    const QVariant var = item->data (0, Qt::UserRole);

    if ( var.canConvert<tree_element_t> () ) {

        te = var.value<tree_element_t> ();
        this->selected_element = te;
        switch ( te.type ) {
        case TE_PEER_ID:
            en_pbn_MQPingClient = true;
            break;
        default:
            break;
        }
        this->addLog (QString::number(te.type));
    }

    this->pbn_MQPingClient->setEnabled (en_pbn_MQPingClient);
    this->spb_MQPingIntLine->setEnabled (en_pbn_MQPingClient);
}
