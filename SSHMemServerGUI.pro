#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T21:58:47
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
lessThan(QT_MAJOR_VERSION, 5): QT += widget

TARGET = SSHMemServerGUI
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../qtcreator_compiled/release/ -lshmIPCS
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../qtcreator_compiled/debug/ -lshmIPCS
else:unix: LIBS += -L$$PWD/../qtcreator_compiled/ -lshmIPCS

INCLUDEPATH += $$PWD/../shmIPCS
DEPENDPATH += $$PWD/../shmIPCS
